# SQLite

Version: 3.26.0

## Overview

* SQLite is a C library that implements an SQL database engine. A large subset of SQL92 is supported. A complete database is stored in a single disk file. The API is designed for convenience and ease of use. Applications that link against SQLite can enjoy the power and flexiblity of an SQL database without the administrative hassles of supporting a separate database server. Because it omits the client-server interaction overhead and writes directly to disk, SQLite is also faster than the big database servers for most operations. In addition to the C library, the SQLite distribution includes a command-line tool for interacting with SQLite databases and SQLite bindings for Tcl/Tk.

## Usage

This sqlite container is inteed to be used as base image for application requiring sqlite.
SQLite official webpage: https://www.sqlite.org/index.html

## Notes

The default user for container is dbuser