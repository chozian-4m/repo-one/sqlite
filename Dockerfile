ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

RUN dnf upgrade -y &&  \
    dnf clean all && \
    rm -rf /var/cache/dnf &&\
    dnf install -y sqlite && \
    useradd -ms /bin/bash dbuser && \
    sqlite3 --version

USER dbuser
    
WORKDIR /home/dbuser

HEALTHCHECK NONE

ENTRYPOINT ["sqlite3"]
